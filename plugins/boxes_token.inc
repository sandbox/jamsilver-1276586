<?php

/**
 * Extends popups so forms can be embedded within
 */
class boxes_token extends boxes_simple {

  public function render() {
    $output = parent::render();
    if (!empty($output['subject'])) {
      $output['subject'] = token_replace($output['subject'], array(), array('clear' => TRUE, 'sanitize' => TRUE));
    }
    if (!empty($output['content'])) {
      $output['content'] = token_replace($output['content'], array(), array('clear' => TRUE, 'sanitize' => TRUE));
    }
    return $output;
  }
}
